# Spring Batch: Resource cleanup with Tasklet

In this project you will learn how to use a tasklet to clean up
a single table using Spring Batch architecture.
package com.cursospring.batch.taskletresourcecleanup.dto;

import lombok.Data;

@Data
public class EmployeeDTO {

    private String employeeId;
    private String firstName;
    private String lastName;
    private int age;
    private String email;
}

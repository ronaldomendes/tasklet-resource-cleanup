package com.cursospring.batch.taskletresourcecleanup.repository;

import com.cursospring.batch.taskletresourcecleanup.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {
}

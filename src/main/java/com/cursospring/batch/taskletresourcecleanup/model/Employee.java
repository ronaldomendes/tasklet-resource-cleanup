package com.cursospring.batch.taskletresourcecleanup.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "tb_employee")
public class Employee {

    @Id
    private String employeeId;
    private String firstName;
    private String lastName;
    private int age;
    private String email;
}

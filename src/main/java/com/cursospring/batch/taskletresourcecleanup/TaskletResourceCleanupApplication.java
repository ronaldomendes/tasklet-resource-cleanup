package com.cursospring.batch.taskletresourcecleanup;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.taskletresourcecleanup"})
public class TaskletResourceCleanupApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskletResourceCleanupApplication.class, args);
    }

}

package com.cursospring.batch.taskletresourcecleanup.job;

import com.cursospring.batch.taskletresourcecleanup.repository.EmployeeRepository;
import com.cursospring.batch.taskletresourcecleanup.tasklet.DataCleanup;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.cursospring.batch.taskletresourcecleanup.utils.Constants.QUALIFIER_NAME;
import static com.cursospring.batch.taskletresourcecleanup.utils.Constants.STEP_NAME;

@Configuration
@AllArgsConstructor
public class DemoJob {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final EmployeeRepository employeeRepository;

    @Qualifier(value = QUALIFIER_NAME)
    @Bean
    public Job demoOneJob() throws Exception {
        return jobBuilderFactory.get(QUALIFIER_NAME)
                .start(stepOneDemo())
                .build();
    }

    private Step stepOneDemo() throws Exception {
        return stepBuilderFactory.get(STEP_NAME)
                .tasklet(new DataCleanup(employeeRepository))
                .build();
    }
}

package com.cursospring.batch.taskletresourcecleanup.tasklet;

import com.cursospring.batch.taskletresourcecleanup.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

@AllArgsConstructor
public class DataCleanup implements Tasklet {

    private final EmployeeRepository employeeRepository;

    @Override
    public RepeatStatus execute(StepContribution stepCont, ChunkContext chunk) throws Exception {
        employeeRepository.deleteAll();
        return RepeatStatus.FINISHED;
    }
}
